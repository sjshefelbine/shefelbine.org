# How to...

## Preview the website

Open a terminal and run `hugo server`, to show pages marked as draft run
`hugo server -D`.

## Change nav bar items

Open `hugo.toml`, locate section `[params]` and setting `nav`. Edit the list to
include the url of the page you want to add, the text on the bar will be the
title of the page.

## Create a new page

Open a terminal and run `hugo new page/path.md`. This will create the file
`content/page/path.md` which will compile to `/page/path.html` edit the
metadata at the top of the file to be what is needed. Delete the line
`draft = true` to make the page appear.

## Create a news item

Follow the steps above to create a new page, have the path be `news/year-season.md`
or similar.

## Add a new person

Open `content/people.md` and add the following template filling out needed text.

```
{{< person "filename(not path)" "Name" >}}
Info
{{< /person >}}
```

## Generate / upload website

Run `hugo`, this will generate all of the pages. They will be in the "public"
directory.

Make sure that the pages repository is cloned into the subdirectory "pages"

Run `rsync --exclude='.git*' -r --delete public/ pages` this will sync all of
the needed files into the hosted repository.

Use git to commit changes to both the `shefelbine.org` and `pages` repositories.
The website will update the pages.

One-liner:

```
rm -rf public && hugo && rsync --exclude='.git*' -r --delete public/ pages && cd pages && git add -A && git commit -m "Rebuild" && git push && cd ..
```
