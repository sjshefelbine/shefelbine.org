+++
title = 'Teaching'
+++
## Courses

### Biomechanics

Core class for bioengineering undergrads covering rigid body dynamics and mechanics of materials.

### Mechanics of Materials

Core class for mechanical engineering undergrads covering stress and strain calculations.

### Musculoskeletal Biomechanics

Graduate level technical elective covering mechanics of the musculoskeletal system, including physiology, structure, function, and mechanics.

### Capstone Design

Senior mechanical engineering required final design project. Teams of students design, build, and test mechanical designs.

### Design in Nature

Currently taught as a summer course in Oxford through the Dialogue of Civilizations program, this course examines evolution as a design process. 

## Awards

 - 2011 Rector’s Medal for Excellence in Teaching
 - 2010 Royal Academy of Engineering Exxon Mobil Teaching Award
 - 2010 Faculty of Engineering Teaching Award
 - 2010 Department of Bioengineering Best Teacher Award 

## Grants

 - Mathworks Curriculum Development Grant: Developing virtual mechanics labs
 - Envision Engineering Education Grant: Bringing the lab to the lecture theater
 - Teaching Development Grant: Mechanics in motion: visualizing physical principles 

