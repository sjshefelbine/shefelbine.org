+++
title = 'People'
+++

{{< person "sandra-shefelbine.webp" "Dr. Sandra Shefelbine" >}}
🇺🇸 / 🇬🇧  
Principal Investigator  
PhD Stanford University  
MPhil Cambridge University  
B.S.E. Princeton University  
{{< /person >}}

{{< person "soha-bentahar.webp" "Soha Ben Tahar">}}
🇫🇷  
PhD Student  
Turing patterns in development  
{{< /person >}}

{{< person "vineel-kondiboyina.webp" "Vineel Kondiboyia">}}
🇮🇳  
PhD Student  
Cartilage mechanobiology in axolotl limbs  
{{< /person >}}

{{< person "quentin-meslier.webp" "Quentin Meslier">}}
🇫🇷  
PhD Student  
Manipulating fluid flow for mechanoadaptation in bone  
{{< /person >}}

## Alumni

### Masters Students:

- [Nicole DiMauro](https://www.linkedin.com/in/nicole-dimauro/)
- [Pranay Kumar Reddy Valluru](https://www.linkedin.com/in/pranay-kumar-reddy-valluru/)
- [Cameron Nurse](https://www.linkedin.com/in/cameron-nurse-4a6970123/)
- [Aowen Deng](https://www.linkedin.com/in/aowendeng/)
- [Daniel Podlisny](https://www.linkedin.com/in/daniel-podlisny/)
- [Emily Man](https://www.linkedin.com/in/emily-man/)
- [Allison Su](https://www.linkedin.com/in/alisonsu/)
- [Mugunthan Udayan](https://www.linkedin.com/in/mugunthanudayan/)
- [Rahul Gawande](https://www.linkedin.com/in/rahulgawande/)
- [Vineel Kondiboyina](https://www.linkedin.com/in/vineelkondiboyina/)
- [Ali Alneami](https://www.linkedin.com/in/ali-alneami-365b0550/)

### PhD Students:

- [Dr. Mahsa Sadeghian](https://www.linkedin.com/in/mahsa-sadeghian-961a4b80/) (2022)
- [Dr. Rachel Horenstein](https://www.linkedin.com/in/rachel-horenstein/) (2020)
- [Dr. Sabah Nobakhti](https://www.linkedin.com/in/sabah-nobakhti-31879568/) (2019)
- [Dr. Judith Piet](https://www.linkedin.com/in/judith-piet-ba98176a/) (2019)
- [Dr. Adina Draghici](https://www.linkedin.com/in/adina-draghici-86533115/) (2017)
- [Dr. Mario Giorgi](https://www.linkedin.com/in/mariogiorgi/) (2015)
- [Dr. Richard Woodward](https://www.linkedin.com/in/rbwoodward/) (2015)
- [Dr. Luis Lamas](https://www.linkedin.com/in/luis-lamas-b7738664/) (2015)
- [Dr. Naiara Rodriguez Florez](https://www.linkedin.com/in/naiara-rodriguez-florez-0b24b127/) (2015)
- [Dr. Michal Klosowski](https://www.linkedin.com/in/michal-klosowski-63619025/) (2014)
- [Dr. Andre Periera](https://www.linkedin.com/in/andreferropereira/) (2014)
- [Dr. Jonathan Moodie](https://www.linkedin.com/in/jonathan-moodie-03b2bb4b/) (2011)
- [Dr. Alessandra Carriero](https://www.linkedin.com/in/alessandra-carriero-7b343017/) (2009)

### Post-doc:

- [Dr. Ester Comellas](https://www.linkedin.com/in/ester-comellas-10911226/) (2019-21)
- [Dr. Johanna Farkas](https://www.linkedin.com/in/johanna-farkas-b9a54032/) (2017-19) 
- [Dr. Joseph Hollmann](https://www.linkedin.com/in/joseph-hollmann-3b093b2/) (2014-15) 
- [Dr. Jean-Philippe Berteau](https://www.linkedin.com/in/jean-philippe-berteau-pt-phd-232381b8/) (2013-14) 
- [Dr. Alessandra Carriero](https://www.linkedin.com/in/alessandra-carriero-7b343017/) (2009-14) 
- [Dr. Warren Macdonald](https://www.linkedin.com/in/warren-macdonald-53521517/) (2008-11)
- [Dr. Michael Doube](https://www.linkedin.com/in/mdoube/?originalSubdomain=hk) (2008-11) 
- [Dr. Maximilien Vanleene](https://www.linkedin.com/in/maximilien-vanleene-41a89959/) (2007-12) 
