+++
title = 'Summer 2019'
date = 2019-08-01
+++

**Ester** and **Mahsa** presented their research at the [Computer Methods in
Biomechanics and Biomedical Engineering (CMBBE 2019)](https://cmbbe2019.com/) in
New York City.
