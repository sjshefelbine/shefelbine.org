+++
title = 'Spring 2020'
date = 2020-06-01
+++

**Rachel Horenstein** successfully defended her PhD dissertation titled
"Mechanisms of cam morphology of the proximal femur". Congratulations Dr.
Horenstein!

Saying goodbye to Soha, Marylou and Julian visiting students from France. We'll
miss you!

![](/news/2020-spring.webp)

**Dr. Ester Comellas** was appointed as an assistant professor at the Department
of Physics in the Polytechnic University of Catalonia (UPC). Congratulations Dr.
Comellas!
