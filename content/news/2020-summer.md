+++
title = 'Summer 2020'
date = 2020-09-01
+++

**Ester**, **Rachel** and **Mahsa** presented their research at Summer
Biomechanics, Bioengineering, and Biotransport Conference (SB3C 2020) in a
virtual meeting format.
