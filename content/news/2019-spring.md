+++
title = 'Spring 2019'
date = 2019-06-01
+++

**Judith Piet** successfully graduated with her PhD! Congratulations **Dr.
Piet**!

![](/news/2019-spring.webp)
