+++
title = 'Advanced Skeletal Technologies'
image = '/research/SkeletalTech.webp'
summary = '''
We are developing new techniques for examining and measuring the musculoskeletal system. We use novel animal models, develop computational methods, and build hardware to explore aspects of the musculoskeletal system.
'''
+++
## Whole mount fluorescence in situ hybridization (FISH)

We are developing methods to measure the molecular response of bone to mechanical load in 3D (whole mount). When a bone cell senses the mechanical load, it stops producing a protein, sclerostin. By fluorescently labelling the molecular signal of sclerostin (mRNA), we can identify regional differences in spatial location of the cellular response.

![Molecular imaging of sclerostin, which is labelled in magenta and shows up as small dots all over the bone in 3D.](/research/WISHBone.webp)

## Turing patterns in 3D

Nature uses chemical reactions to create patterns over large distances. As chemicals react with each other, stable patterns with regions of high and low concentrations result. This is the basis for spots and stripes on animal skins. We have explored how patterns develop in 3D, which may help to explain how (and where) joints form early in development.
Collaborators: Ester Comellas and Jose Munoz

![3D cubes which show conentrations of molecules. The concentrations form patterns: a plane, cylinders, a sphere ](/research/TuringPattern.webp)


## Machine learning to count cells in 3D

Light sheet microscopy produces 3D images of tissues. We compared opensource machine learning algorithms, Cellpose and Stardist, in useability for detecting cells in 3D images of regenerating axolotl limbs. 

![Identifying cells in 3D images requires segmentation. This image shows ellipsoidal cells in a 3D projection. ](/research/ColorfulCellSurface.webp)


## Previous Projects

### Muscle Mechanomyography

Muscles not only give electrical signals (EMG), but also mechanical signals (MMG) during contractions. We have developed a sensor to measure the muscle contractions and motion pervasively using MMG.
Collaborators: Ravi Vaidyanathan, Imperial College London

Woodward, R. B., Stokes, M. J., S. J. Shefelbine, Vaidyanathan, R. (2019). [Segmenting Mechanomyography Measures of Muscle Activity Phases Using Inertial Data.](https://www.nature.com/articles/s41598-019-41860-4) _Scientific reports_ 9 (1), 5569  

### Nanowell array electrodes to measure bone biomarkers

Analysis of bone biomarkers (proteins in blood that indicate bone formation or resorption) often requires sending blood samples to a specialized lab for analysis. We developed a nanowell array electrode to measure alkaline phosphatase. The nanowell array had high specificity and required less sample volume than traditional techniques.
Collaborator: HeaYeon Lee, Mara Technologies

Lee, J., Bubar, C., Moon, H., Kim, J., Busnaina, A., Lee, H. Y., & **S. J. Shefelbine** (2018). [Measuring Bone Biomarker Alkaline phosphatase with Wafer-scale Nanowell array Electrodes](https://www.ncbi.nlm.nih.gov/pubmed/30460852). _ACS sensors_.

### Three dimensional bone histology

Combining a microtome with a fluorescence microscope we have obtain 3D images of calcien labels in bone indicating regions of recent bone formation.
Collaborations: Department of Bioengineering (Imperial College)

Carriero, A., Pereira, A. F., Wilson, A. J., Castagno, S., Javaheri, B., Pitsillides, A. A., **S. J. Shefelbine** (2018). [Spatial relationship between bone formation and mechanical stimulus within cortical bone: Combining 3D fluorochrome mapping and poroelastic finite element modelling](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5997173/). _Bone Reports_, 8, 72–80.  
  