+++
title = 'Mechanoadaptation in Skeletal Tissues'
image = '/research/Mechanobiology.webp'
summary = '''
Bone adapts to the mechanical environment, building bone with increased load and resorbing bone with decreased load. We are exploring the effects across length scales (whole bone, tissue, cell, molecule) in humans and animal models.
'''
+++

Mechanoadaptation is the bone's ability to respond to mechanical load. Forces applied to the bone at the whole bone level due to muscle contractions and joint loads create stresses and strains in the tissue. The osteocytes (bone cells) detect the mechanical environment and initiate a molecular response, which builds more bone tissue and makes the bone stronger. 

![Image of the different legnth scales of mechanobiology: whole bone, tissue, cellular and molecular.](/research/MechanoMultiscale.webp)


## Mechanoadaptation in bone

Using a mouse tibial loading model (external load placed on the tibia), we are exploring the mechanical signal the bone cells sense in their environment. We used finite element modeling to determine the fluid flow under different loading conditions. We have shown that fluid flow can induce adaptation even under small strain. 

![A finite element model of bone showing longitudinal strain and peak fluid flow during loading.](/research/FluidFlow.webp)

## Mechanotransduction in cartilage and bone: calcium signaling

Mechanotransduction is the translation of the mechanical signal into cellular signal. The first response of bone and cartilage cells to a mechanical signal is an influx of calcium to the cell. By making the cellular calcium fluorescent and applying load under a microscope we can watch the cells transduce the mechanical signal.

# Previous projects

## Mechanobiology of joint morphogenesis

How do mechanics affect joint formation? We combine in vivo experiments that block mechanical signals in regenerating axolotl limbs with a finite element model that links the biomechanics and biochemistry of limb development to explore the role of mechanics during limb development.
Collaborator: Professor James Monaghan (Northeastern University)

![A lightsheet image is segemented to measure joint shape and create a finite element model.](/research/JointMorphogenesis.webp)

## Effects of mechanical loading in endochondral ossification

Mechanics plays an important role in regulation of cartilage growth and ossification. In this project, an algorithm has been developed to predict the endochondral ossification pattern in the epiphysis of multiple bones and investigate the influence of joint contact forces on the shape of the developing bones. The aim of this study is to explain the onset of developmental deformities, such as cam-type FAI, caused by mechanical loads and the consequent growth abnormalities.

![A finite element model simulating endochondral ossification.](/research/ossification_FE_Mahsa.webp)
