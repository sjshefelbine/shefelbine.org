+++
title = 'Multiscale Mechanics of Skeletal Tissues'
image = '/research/Mechanics.webp'
summary = '''
Bone as a material is strong (carry a lot of load) and tough (bend without breaking). The toughness results from organization of the bone matrix from the molecular level to the whole bone. We explore how pathological changes, such as genetic defects, affect mechanical properties of the bone.
'''
+++
## Mechanics of a regenerating axolotl limb

Axolotls regenerate their limbs. We are examining the mechanical properties of the tissue during regeneration using indentation and inverse finite element modeling. This will help us understand how the mechanical environment of the cells develops.
Collaborator: James Monaghan

![This figure shows different stages of development of a regenerating limb bud. A graph of a stress relaxation curve (constant displacement applied) starts with a high initial stress that exponenetially drops over 60 seconds to a equilibrium stress.](/research/RegenerativeMechanics.webp)

## Osteon organization in osteogenesis imperfecta

Bone is organized in osteons (rings of bone surrounding a blood vessel). In the brittle bone disease osteogenesis imperfecta, which is caused by a genetic defect in collagen, the rings are thinner. This may contribute to the brittleness of the bone.
Collaborator:  Frederic Shapiro

![Histological images of normal and OI bone. The normal bone has lamealla (lines of bone) that are thicker than the OI bone.](/research/OIhistology.webp)

## Vaping decreases bone toughness

We used a mouse of e-cigarrete exposure to determine the effects of vaping on bone. Electronic cigarettes alters bone structure and decreases the mechanical properties. 
Collaborators: Chiara Bellini and Jessica Oakes

![Histological images of normal and OI bone. The normal bone has lamealla (lines of bone) that are thicker than the OI bone.](/research/VapeMechanics.webp)

## Multiscale toughness of bone

Toughness in bone arises from several mechanisms which act at multiple length scales in the bone hierarchical structure. In order to  investigate the toughening mechanisms in bone, various parameters are measured in pathalogic mouse models of bone and related across the length-scales. 

![Toughness testing of the whole bone shows a bone with a crack through it. At the tissue level, there is a map of elastic modulus across the cross section of a bone. At the fibril level there is mineral on collagen fibrils. A Raman spectra shows peaks corresponding to bone composition.  ](/research/Multiscale.webp)

# Previous Projects

## Bone scaling across species
Across animal species bone must support varied mass and locomotor activities. In this project we looked at bone scaling in the appendicular skeleton in felids (cats), artiodactyls (hoofed animals), macropods (kangaroos), and bipedal birds to understand how bone structure changes in response to increases in body size and locomotor habits.
Collaborators: Dr. John Hutchinson (Royal Veterinary College)

![A tiger bone in cross section. Samples of trabecular bone from different species. ](/research/AcrossSpecies.webp)

Doube, M., Felder, A. A., Chua, M. Y., Lodhia, K., Kłosowski, M. M., Hutchinson, J. R., & S. J. Shefelbine** (2018). [Limb bone scaling in hopping macropods and quadrupedal artiodactyls](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6227981/). _Royal Society Open Science_, 5(10), 180152.  

M. Doube, S.C.W. Yen, M.M. Klosowski, A.A. Farke, J.R. Hutchinson, S. J. Shefelbine**, (2012). [Whole-bone scaling of the avian pelvic limb.](http://www.ncbi.nlm.nih.gov/pubmed/22606941) _J Anatomy_ 221(1): 21-9.  

M. Doube, M Kłosowski, A. M. Wiktorowicz-Conroy, J. R. Hutchinson, S. J. Shefelbine, (2011). [Trabecular bone scales allometrically in mammals and birds.](http://www.ncbi.nlm.nih.gov/pubmed/21389033) _Proceedings of the Royal Society Biology B_ 278:3067-73.  

## Characterizing fracture toughness in murine bones
Assessing bone quality is critical in understanding pathology and determining efficacy of therapy. Fracture toughness is a measure of how easy it is to break a bone. In this project we developed methods for creating resistance curves and calculating fracture toughness in mouse bones.
Collaborators: Professor Rob Ritchie (U.C. Berkeley)

![Broken wild type bone has rough surface. Broken osteogenesis imperfecta bone has smooth surface. ](/research/Crackpath.webp)

A. Carriero, E.A. Zimmerann, S. J. Shefelbine, & R.O. Ritchie, (2014). [A methodology for the investigation of toughness and crack propagation in a mouse bone.](http://www.sciencedirect.com/science/article/pii/S1751616114001817) _Jouranl of the Mechanical Behavior of Biomedical Physics_ 39: 38-47.  

A. Carriero, E.A. Zimmermann, A. Paluszny, S.Y. Tang, H. Bale, B. Busse, T. Alliston, G. Kazakia, R.O. Ritchie, S. J. Shefelbine, (2014). [How tough is Brittle Bone? Investigating Osteogenesis Imperfecta in Mouse Bone](http://www.ncbi.nlm.nih.gov/pubmed/24420672). _J Bone Miner Res_ 29(6): 1392–1401.  




