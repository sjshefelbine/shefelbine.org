+++
title = 'Publications'
+++

Chow J, Ryan N, **Shefelbine SJ**, Shapiro F (2023) [Lamellar thickness measurements in control and osteogenesis imperfecta human bone, with development of a method of automated thickness averaging to simplify quantitation.](https://link.springer.com/article/10.1007/s11914-023-00776-9) _Matrix Biol J Int Soc Matrix Biol_ 116:85–101. 

Meslier QA, **Shefelbine SJ** (2023) [Using Finite Element Modeling in Bone Mechanoadaptation.](https://link.springer.com/article/10.1007/s11914-023-00776-9) _Curr Osteoporos Rep_ 21:105–116. 

Comellas E, **Shefelbine SJ** (2022) [The role of computational models in mechanobiology of growing bone.](https://www.frontiersin.org/articles/10.3389/fbioe.2022.973788/full) _Front Bioeng Biotechnol_ 10:973788. 

Meslier QA, DiMauro N, Somanchi P, Nano S, **Shefelbine SJ** (2022) [Manipulating load-induced fluid flow in vivo to promote bone adaptation.](https://doi.org/10.1016/j.bone.2022.116547) _Bone_ 165:116547. 

Piet J, Adamo S, Hu D, Baron R, **Shefelbine SJ** (2022) [Marrow aspiration in aged mice: intramedullary osteogenesis, reduced mechano-adaptation, increased marrow fat.](https://doi.org/10.1080/03008207.2019.1698557) _Connect Tissue Res_ 63:97–111. 

Comellas E, Farkas JE, Kleinberg G, Lloyd K, Mueller T, Duerr TJ, Muñoz JJ, Monaghan JR, **Shefelbine SJ** (2022) [Local mechanical stimuli correlate with tissue growth in axolotl salamander joint morphogenesis.](https://doi.org/10.1098/rspb.2022.0621) _Proc Biol Sci_ 289:20220621. 

Kleinberg G, Wang S, Comellas E, Monaghan JR, **Shefelbine SJ** (2022) [Usability of deep learning pipelines for 3D nuclei identification with Stardist and Cellpose.](https://doi.org/10.1016/j.cdev.2022.203806) _Cells Dev_ 172:203806.

Ben Tahar S, Garnier J, Eller K, DiMauro N, Piet J, Mehta S, Bajpayee AG, **Shefelbine SJ** (2023) [Adolescent obesity incurs adult skeletal deficits in murine induced obesity model.](https://doi.org/10.1002/jor.25378) _J Orthop Res_ 41:386–395. 

Aldegaither N, Sernicola G, Mesgarnejad A, Karma A, Balint D, Wang J, Saiz E, **Shefelbine SJ**, Porter AE, Giuliani F (2021) [Fracture toughness of bone at the microscale.](https://www.sciencedirect.com/science/article/abs/pii/S1742706120307212?via%3Dihub) _Acta Biomater_ 121:475-483 . 

Sadeghian SM, Shapiro FD, **Shefelbine SJ** (2021) [Computational model of endochondral ossification: Simulating growth of a long bone.](https://www.sciencedirect.com/science/article/abs/pii/S8756328221002970) _Bone_ 153:116132 . 

Depalle B, McGilvery CM, Nobakhti S, Aldegaither N, **Shefelbine SJ**, Porter AE (2021) [Osteopontin regulates type I collagen fibril formation in bone tissue.](https://www.sciencedirect.com/science/article/pii/S1742706120302373) _Acta Biomater_ 120:194-202 . 

Mehta S, Young CC, Warren MR, Akhtar S, **Shefelbine SJ**, Crane JD, Bajpayee AG (2021) [Resveratrol and Curcumin Attenuate Ex Vivo Sugar-Induced Cartilage Glycation, Stiffening, Senescence, and Degeneration.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8804818/) _Cartilage_ 13:1214S-1228S . 

Horenstein RE, Meslier Q, Spada JA, Halverstadt A, Lewis CL, Gimpel M, Birchall R, Wedatilake T, Fernquest S, Palmer A, Glyn-Jones S, **Shefelbine, SJ** (2021) [Measuring 3D growth plate shape: Methodology and application to cam morphology.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8222423/) _J Orthop Res_ 39:2398-2408. 

Eller K, DiMauro N, Garnier J, Ruberti A, Meslier Q, Piet J, **Shefelbine SJ** (2021) [Mechanoadaptation of the bones of mice with high fat diet induced obesity in response to cyclical loading.](https://www.sciencedirect.com/science/article/abs/pii/S0021929021003493?via%3Dihub) _J Biomech_ 124:110569. 

Kainz H, Killen BA, Van Campenhout A, Desloovere K, Garcia Aznar JM, **Shefelbine SJ**, Jonkers I (2021) [ESB Clinical Biomechanics Award 2020: Pelvis and hip movement strategies discriminate typical and pathological femoral growth - Insights gained from a multi-scale mechanobiological modelling framework.](https://www.sciencedirect.com/science/article/pii/S0268003321001352) _Clin Biomech_ 87:105405.

Carriero A, Javaheri B, Bassir Kazeruni N, Pitsillides AA, **Shefelbine SJ** (2021) [Age and Sex Differences in Load-Induced Tibial Cortical Bone Surface Strain Maps.](https://asbmr.onlinelibrary.wiley.com/doi/full/10.1002/jbm4.10467?af=R) _JBMR Plus_ 5:e10467 

Zando RB, Mesgarnejad A, Pan C, **Shefelbine SJ**, Karma A, Erb RM (2020) [Enhanced toughness in ceramic-reinforced polymer composites with herringbone architectures.](https://www.sciencedirect.com/science/article/abs/pii/S0266353820323034) _Compos Sci Technol_ 108513.

Duerr T.J., Comellas E., Jeon E.K., Farkas J.E., Joetzjer M., Garnier J., **Shefelbine S.J.**, Monaghan J.R. (2020) [3D Visualization of Macromolecule Synthesis.](https://elifesciences.org/articles/60354) _eLife_ 9: e60354.

Horenstein R.E., Goudeau Y.R., Lewis C.L., **Shefelbine S.J.** (2020) [Using Magneto-Inertial Measurement Units to Pervasively Measure Hip Joint Motion during Sports.](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7506643/) _Sensors_ 20(17): 4970.

Mesgarnejad A., Pan C., Erb R.M., Shefelbine S.J., Karma A. (2020) [Crack path selection in orientationally ordered composites.](https://journals.aps.org/pre/abstract/10.1103/PhysRevE.102.013004) _Phys Rev E_ 102:013004 . 

Kainz H., Killen B.A., Wesseling M., Perez-Boerema F., Pitto L., Garcia Aznar J.M., **Shefelbine S.J.**, Jonkers I. (2020) [A multi-scale modelling framework combining musculoskeletal rigid-body simulations with adaptive finite element analyses, to evaluate the impact of femoral geometry on hip joint contact forces and femoral bone growth.](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0235966) _PloS One_ 15:e0235966 . 

Sadeghian, S. M., Lewis, C. L., **S. J. Shefelbine** (2020). [Predicting growth plate orientation with altered hip loading: potential cause of cam morphology.](https://link.springer.com/article/10.1007/s10237-019-01241-2) _Biomechanics and modeling in mechanobiology_, 1-12.  
  
Main, R. P., **S. J. Shefelbine**, Meakin, L. B., Silva, M. J., van der Meulen, M. C., & Willie, B. M. (2019). [Murine Axial Compression Tibial Loading Model to Study Bone Mechanobiology: Implementing the Model and Reporting Results.](https://onlinelibrary.wiley.com/doi/abs/10.1002/jor.24466) _Journal of Orthopaedic Research_.  
  
Connizzo, B. K., Piet, J. M., **S. J. Shefelbine**, & Grodzinsky, A. J. (2019). [Age-associated changes in the response of tendon explants to stress deprivation is sex-dependent.](https://www.tandfonline.com/doi/full/10.1080/03008207.2019.1648444) _Connective tissue research_, 1-15.  
  
Kondiboyina, V., Raine, L. B., Kramer, A. F., Khan, N. A., Hillman, C. H., & **S. J. Shefelbine** (2020). [Skeletal Effects of Nine Months of Physical Activity in Obese and Healthy-weight Children.](https://insights.ovid.com/crossref?an=00005768-900000000-96510) _Medicine and science in sports and exercise._  
  
Horenstein, R. E., Lewis, C. L., Yan, S., Halverstadt, A.L. & **S. J. Shefelbine** (2019). [Validation of Magneto-Inertial Measuring Units for Measuring Hip Joint Angles.](https://www.sciencedirect.com/science/article/pii/S0021929019303616) _Journal of Biomechanics_, 91C, 170-174.  
  
Woodward, R. B., Stokes, M. J., **S. J. Shefelbine**, Vaidyanathan, R. (2019). [Segmenting Mechanomyography Measures of Muscle Activity Phases Using Inertial Data.](https://www.nature.com/articles/s41598-019-41860-4) _Scientific reports_ 9 (1), 5569  
  
Piet, J., Hu, D., Baron, R., & **S. J. Shefelbine** (2019). [Bone adaptation compensates resorption when sciatic neurectomy is followed by low magnitude induced loading.](https://www.sciencedirect.com/science/article/pii/S8756328218304678) _Bone_ 120, 487-494  
  
Doube, M., Felder, A. A., Chua, M. Y., Lodhia, K., Kłosowski, M. M., Hutchinson, J. R., & **S. J. Shefelbine** (2018). [Limb bone scaling in hopping macropods and quadrupedal artiodactyls](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6227981/). _Royal Society Open Science_, 5(10), 180152.  
  
Lee, J., Bubar, C., Moon, H., Kim, J., Busnaina, A., Lee, H. Y., & **S. J. Shefelbine** (2018). [Measuring Bone Biomarker Alkaline phosphatase with Wafer-scale Nanowell array Electrodes](https://www.ncbi.nlm.nih.gov/pubmed/30460852). _ACS sensors_.  
  
Chadwick, K. P., Mueske, N. M., Horenstein, R. E., **S. J. Shefelbine**, & Wren, T. A. L. (2018). [Children with myelomeningocele do not exhibit normal remodeling of tibia roundness with physical development](https://europepmc.org/abstract/med/29991457). _Bone_, 114, 292–297.  
  
Márquez-Flórez, K., **S. J. Shefelbine**, Ramírez-Martínez, A., & Garzón-Alvarado, D. (2018). [Computational model for the patella onset](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0207770). _PloS One_, 13(12), e0207770.  
  
Carriero, A., Pereira, A. F., Wilson, A. J., Castagno, S., Javaheri, B., Pitsillides, A. A., **S. J. Shefelbine** (2018). [Spatial relationship between bone formation and mechanical stimulus within cortical bone: Combining 3D fluorochrome mapping and poroelastic finite element modelling](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5997173/). _Bone Reports_, 8, 72–80.  
  
Kłosowski, M. M., Carzaniga, R., **S. J. Shefelbine**, Porter, A. E., & McComb, D. W. (2018). [Nanoanalytical electron microscopy of events predisposing to mineralisation of turkey tendon](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5813010/). _Scientific Reports_, 8(1), 3024.  
  
S. Nobakhti, and **S. J. Shefelbine**, (2018). [On the Relation of Bone Mineral Density and the Elastic Modulus in Healthy and Pathologic Bone](https://www.ncbi.nlm.nih.gov/pubmed/29869752). _Current osteoporosis reports_ 16(4): 404-410.  
  
Javaheri, B., Carriero, A., Wood, M., De Souza, R., Lee, P. D., **S. J. Shefelbine**, & Pitsillides, A. A. (2018). [Transient peak-strain matching partially recovers the age-impaired mechanoadaptive cortical bone response](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5924380/). _Scientific Reports_, 8(1), 6636.  
  
Sheehan, F. T., Brainerd, E. L., Troy, K. L., **S. J. Shefelbine**, & Ronsky, J. L. (2018). [Advancing quantitative techniques to improve understanding of the skeletal structure-function relationship](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5859431/). _Journal of Neuroengineering and Rehabilitation_, 15(1), 25.  
  
Márquez-Flórez, K. M., Monaghan, J. R., **S. J. Shefelbine**, Ramirez-Martínez, A., & Garzón-Alvarado, D. A. (2018). [A computational model for the joint onset and development](http://europepmc.org/abstract/MED/29653160). _Journal of Theoretical Biology_, 454, 345–356.  
  
F. Coustry, K.L. Posey, T. Maerz, K. Baker, A.M. Abraham, C.G. Ambrose, S. Nobakhti, **S. J. Shefelbine**, et al., (2018). [Mutant cartilage oligomeric matrix protein (COMP) compromises bone integrity, joint function and the balance between adipogenesis and osteogenesis](https://www.ncbi.nlm.nih.gov/pubmed/29309831). _Matrix Biology_ 67: 75-89.  
  
Novais, E. N., **S. J. Shefelbine**, Kienle, K.-P., Miller, P. E., Bowen, G., Kim, Y.-J., & Bixby, S. D. (2018). [Body Mass Index Affects Proximal Femoral but Not Acetabular Morphology in Adolescents Without Hip Pathology](https://journals.lww.com/jbjsjournal/fulltext/2018/01030/Body_Mass_Index_Affects_Proximal_Femoral_but_Not.9.aspx). _The Journal of Bone and Joint Surgery_. American Volume, 100(1), 66–74.  
  
Woodward, R. B., **S. J. Shefelbine**, & Vaidyanathan, R. (2017). [Pervasive Monitoring of Motion and Muscle Activation: Inertial and Mechanomyography Fusion](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7983445). _IEEE/ASME Transactions on Mechatronics_, 22(5), 2022–2033.  
  
DeSouza, R., Javaheri, B., Collinson, R. S., Chenu, C., **S. J. Shefelbine**, Lee, P. D., & Pitsillides, A. A. (2017). [Prolonging disuse in aged mice amplifies cortical but not trabecular bones’ response to mechanical loading](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5601267/). _Journal of Musculoskeletal & Neuronal Interactions_, 17(3), 218–225  
  
Chadwick, K. P., **S. J. Shefelbine**, Pitsillides, A. A., & Hutchinson, J. R. (2017). [Finite-element modelling of mechanobiological factors influencing sesamoid tissue morphology in the patellar tendon of an ostrich](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5493912/). _Royal Society Open Science_, 4(6), 170133.  
  
Yadav, P., **S. J. Shefelbine**, Pontén, E., & Gutierrez-Farewik, E. M. (2017). [Influence of muscle groups’ activation on proximal femoral growth tendency](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5671539/). _Biomechanics and Modeling in Mechanobiology_, 16(6): 1869-1883.  
  
Draghici, A. E., Potart, D., Hollmann, J. L., Pera, V., Fang, Q., DiMarzio, C. A., Taylor, J. A., Niedre, M. J., & **S. J. Shefelbine** (2017). [Near Infrared Spectroscopy for Measuring Changes in Bone Hemoglobin Content after Exercise in Individuals with Spinal Cord Injury](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5711624/). _Journal of Orthopaedic Research_, 36(1): 183-191.  
  
A.E. Draghici, G. Picard, J.A. Taylor, **S. J. Shefelbine**, (2017). [Assessing kinematics and kinetics of functional electrical stimulation rowing](http://www.sciencedirect.com/science/article/pii/S0021929017300088). _Journal of Biomechanics_, 28(53):120-126.  
  
Nguyen, M., Singhal, P., Piet, J. W., **S. J. Shefelbine**, Maden, M., Voss, S. R., & Monaghan, J. R. (2017). [Retinoic acid receptor regulation of epimorphic and homeostatic regeneration in the axolotl](https://dev.biologists.org/content/144/4/601.long). _Development_, 144(4), 601–611.  
  
Castro-Abril, H. A., Guevara, J. M., Moncayo, M. A., **S. J. Shefelbine**, Barrera, L. A., & Garzón-Alvarado, D. A. (2017). [Cellular scale model of growth plate: An in silico model of chondrocyte hypertrophy](https://europepmc.org/abstract/med/28526527). _Journal of Theoretical Biology_, 428, 87–97.  
  
Berteau, J.-P., Oyen, M., & **S. J. Shefelbine** (2016). [Permeability and shear modulus of articular cartilage in growing mice](https://link.springer.com/article/10.1007%2Fs10237-015-0671-3). _Biomechanics and Modeling in Mechanobiology_, 15(1), 205–212.  
  
Depalle, B., Qin, Z., **S. J. Shefelbine**, & Buehler, M. J. (2016). [Large Deformation Mechanisms, Plasticity, and Failure of an Individual Collagen Fibril With Different Mineral Content](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4915725/). _Journal of Bone and Mineral Research_ 31(2), 380–390.  
  
Javaheri, B., Hopkinson, M., Poulet, B., Pollard, A. S., **S. J. Shefelbine**, Chang, Y.-M., Francis-West, P., Bou-Gharios, G., & Pitsillides, A. A. (2016). [Deficiency and Also Transgenic Overexpression of Timp-3 Both Lead to Compromised Bone Mass and Architecture In Vivo](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4982603/). _PloS One_, 11(8), e0159657.  
  
Li, T., Chang, S.-W., Rodriguez-Florez, N., Buehler, M. J., **S. J. Shefelbine**, Dao, M., & Zeng, K. (2016). [Studies of chain substitution caused sub-fibril level differences in stiffness and ultrastructure of wildtype and oim/oim collagen fibers using multifrequency-AFM and molecular modeling](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5040355/). _Biomaterials_, 107, 15–22.  
  
Osterhoff, G., Morgan, E. F., **S. J. Shefelbine**, Karim, L., McNamara, L. M., & Augat, P. (2016). [Bone mechanical properties and changes with osteoporosis](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4955555/). Injury, 47 Suppl 2, S11-20.  
  
Poulet, B., Liu, K., Plumb, D., Vo, P., Shah, M., Staines, K., Sampson, A., Nakamura, H., Nagase, H., Carriero, A., **S. J. Shefelbine**, Pitsillides, A. A., & Bou-Gharios, G. (2016). [Overexpression of TIMP-3 in Chondrocytes Produces Transient Reduction in Growth Plate Length but Permanently Reduces Adult Bone Quality and Quantity](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0167971). _PloS One_, 11(12), e0167971.  
  
Ranzoni, A. M., Corcelli, M., Hau, K.-L., Kerns, J. G., Vanleene, M., **S. J. Shefelbine**, Jones, G. N., Moschidou, D., Dala-Ali, B., Goodship, A. E., De Coppi, P., Arnett, T. R., & Guillot, P. V. (2016). Counteracting bone fragility with human amniotic mesenchymal stem cells. _Scientific Reports_, 6, 39656.  
  
Rodriguez-Florez, N., Carriero, A., & **S. J. Shefelbine** (2016). [The use of XFEM to assess the influence of intra-cortical porosity on crack propagation](https://www.tandfonline.com/doi/full/10.1080/10255842.2016.1235158). _Computer Methods in Biomechanics and Biomedical Engineering_, 1–8.  
  
Yadav, P., **S. J. Shefelbine**, & Gutierrez-Farewik, E. M. (2016). [Effect of growth plate geometry and growth direction on prediction of proximal femoral morphology](https://www.sciencedirect.com/science/article/pii/S0021929016303736?via%3Dihub). _Journal of Biomechanics_, 49(9), 1613–1619.  
  
A.K. Oestreich, M.R. Garcia, X. Yao, F.M. Pfeiffer, S. Nobakhti, **S. J. Shefelbine**, ... & C.L. Phillips, (2015). [Characterization of the MPS IH knock-in mouse reveals increased femoral biomechanical integrity with compromised material strength and altered bone geometry](http://www.sciencedirect.com/science/article/pii/S2214426915300288). _Molecular Genetics and Metabolism Reports_ 5: 3-11.  
  
O.G. Andriotis, S.W. Chang, M. Vanleene, P.H. Howarth, D.E. Davies, **S. J. Shefelbine,** M.J. Buehler, & P.J. Thurner, (2015). [Structure-mechanics relationships of collagen fibrils in the osteogenesis imperfecta mouse model.](http://rsif.royalsocietypublishing.org/content/12/111/20150701.long) _Journal of the Royal Society, Interface/ the Royal Society_ 12: 111.  
  
A.F. Pereira, B. Javaheri, A.A. Pitsillides, & **S. J. Shefelbine,** (2015). [Predicting cortical bone adaptation to axial loading in the mouse tibia.](http://rsif.royalsocietypublishing.org/content/12/110/20150590.long) _Journal of the Royal Society, Interface / the Royal Society_ 12: 110.  
  
B. Javaheri, A. Carriero, K.A. Staines, Y.M. Chang, D.A. Houston, K.J. Oldknow, J.L. Millán, B.N. Kazeruni, P. Salmon, **S. J. Shefelbine,** C. Farquharson, & A.A. Pitsillides, (2015). [Phospho1 deficiency transiently modifies bone architecture yet produces consistent modification in osteocyte differentiation and vascular porosity with ageing.](http://www.sciencedirect.com/science/article/pii/S8756328215003099) _Bone_ 81: 277-91.  
  
M. Giorgi, A. Carriero, **S. J. Shefelbine**, & N.C. Nowlan, (2015). [Effects of normal and abnormal loading conditions of morphogenesis of the prenatal hip joint: application to hip dysplasia.](http://www.sciencedirect.com/science/article/pii/S0021929015003395) _Journal of Biomechanics,_ 48: 12.  
  
M.M. Klosowki, R.J. Friederichs, R. Nichol, N. Antolin, R. Carzaniga, W. Windl, S.M. Best, **S. J. Shefelbine**, D.W. McComb, & A.E. Porter, (2015). [Probing carbonate in bone forming materials on the nanometre scale.](http://www.sciencedirect.com/science/article/pii/S1742706115001609) _Acta Biomaterialia_, 20: 129- 139.  
  
J.P. Berteau, M. Oyen, & **S. J. Shefelbine**, (2015). [Permeability and shear modulus of articular cartilage in growing mice.](http://link.springer.com/article/10.1007%2Fs10237-015-0671-3) _Biomechanics and Modeling in Mechanobiology_ 1-8.  
  
R.E. Horenstein, **S. J. Shefelbine**, N.M. Mueske, C.L. Fisher and T.A. Wren, (2015). [An approach for determining quantitative measures for bone volume and bone mass in the pediatric spina bifida population.](http://www.ncbi.nlm.nih.gov/pubmed/26002057) _Clin Biomech_ 30(7): 748–754.  
  
A. Carriero, J.L. Bruse, K.J. Oldknow, J.L> Millán, C. Farquharson, & S. J. Shefelbine, (2014). [Reference point indentation is not indicative of whole mouse bone measures of stress intensity fracture toughness](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4228060/). _Bone_ 69: 174-9.  
  
B. Depalle, Z. Qin, **S. J. Shefelbine**, & M.J. Buehler, (2014). [Influence of cross-link structure, density and mechanical properties in the mesoscale deformation mechanisms of collagen fibrils.](http://www.sciencedirect.com/science/article/pii/S175161611400201X) _Journal of the Mechanical Behavior of Biomedical Materials_ 52:1-13.  
  
A. Carriero, E.A. Zimmerann, **S. J. Shefelbine,** & R.O. Ritchie, (2014). [A methodology for the investigation of toughness and crack propagation in a mouse bone.](http://www.sciencedirect.com/science/article/pii/S1751616114001817) _Jouranl of the Mechanical Behavior of Biomedical Physics_ 39: 38-47.  
  
J.P. Berteau, M. Oyen, **S. Shefelbine,** (2014). [In vitro characterization of the elasticity and the permeability of the mouse cartilage during growth using microindentation](http://www.tandfonline.com/doi/full/10.1080/10255842.2014.931129#.VjvPXE3lu70). _Computer Methods in Biomechanics and Biomedical Engineering_ 17: 68-9.  
  
N. Rodriguez-Florez, E. Garcia-Tunon, Q. Mukadam, E. Saiz, K.J. Oldknow, C. Farquharson, J.L. Millán, A. Boyde and **S. J. Shefelbine**, (2014). [An investigation of the mineral in ductile and brittle cortical mouse bone.](http://www.ncbi.nlm.nih.gov/pubmed/25418329) _J Bone Miner. Res._ 30(5): 786–795.  
  
A. Carriero, L. Abela, A.A. Pitsillides, **S. J. Shefelbine**, (2014). [Ex vivo determination of bone tissue strains for an in vivo mouse tibial loading model.](http://www.ncbi.nlm.nih.gov/pubmed/24835472) _J of Biomechanics_ 47(10): 2490-7.  
  
M. Giorgi, A. Carriero, **S. J. Shefelbine**, N.C. Nowlan, (2014). [Mechanobiological simulations of prenatal joint morphogenesis](http://www.ncbi.nlm.nih.gov/pubmed/24529755). _J of Biomechanics_ 47(5): 989-95.  
  
N. Rodriguez-Florez, M.L. Oyen, **S. J. Shefelbine**, (2014). [Age related changes in mouse bone permeability](http://www.ncbi.nlm.nih.gov/pubmed/24433671). _J of Biomechanics_ 47(5): 1110-16.  
  
G.N. Jones, D. Moschidou, H. Abdulrazzak, B.S. Kalirai, M. Vanleene, S. Osatis, **S. J. Shefelbine**, N.J. Horwood, M. Marenzana, P. De Coppi, J.H. Bassett, G.R. Williams, N.M. Fisk, P.V. Guillot, (2014). [Potential of human fetal chorionic stem cells for the treatment of osteogenesis imperfecta](http://www.ncbi.nlm.nih.gov/pubmed/24028330). _Stem Cells Dev_ 23(3): 262-76.  
  
A. Carriero, E.A. Zimmermann, A. Paluszny, S.Y. Tang, H. Bale, B. Busse, T. Alliston, G. Kazakia, R.O. Ritchie, **S. J. Shefelbine**, (2014). [How tough is Brittle Bone? Investigating Osteogenesis Imperfecta in Mouse Bone](http://www.ncbi.nlm.nih.gov/pubmed/24420672). _J Bone Miner Res_ 29(6): 1392–1401.  
  
A. Carriero, M. Doube, M. Vogt, B. Busse, J. Zustin, A. Levchuk, P. Schneider, R. Müller and **S. J. Shefelbine**, (2014). [Altered lacunar and vascular porosity in osteogenesis imperfecta mouse bone as revealed by synchrotron tomography contributes to bone fragility](http://www.ncbi.nlm.nih.gov/pubmed/24373921). _Bone_ 61:116-24.  
  
A. Pereira, **S. J. Shefelbine**, (2014). [The influence of load repetition in bone mechanotransduction using poroelastic finite element models: The impact of permeability](http://www.ncbi.nlm.nih.gov/pubmed/23689800). _Biomechanics and Modeling in Mechanobiology_ 13(1): 215-25.  
  
W. Macdonald and **S. J. Shefelbine**, (2013). [Characterising neovascularisation in fracture healing with laser Doppler and micro-CT scanning](http://www.ncbi.nlm.nih.gov/pubmed/23881721). _Med Biol Eng Comput_ 51: 1157-65.  
  
M. Vanleene, **S. J. Shefelbine**, (2013). [Therapeutic impact of low amplitude high frequency whole body vibrations on the osteogenesis imperfecta mouse bone](http://www.ncbi.nlm.nih.gov/pubmed/23352925). _Bone_ 53(2): 507-14  
  
B. Poulet, T.A.T. Westerhof, R. Hamilton, **S. J. Shefelbine**, A.A. Pitsillides, (2013). [Spontaneous osteoarthritis in Str/ort mice is unlikely due to greater vulnerability to mechanical trauma](http://www.ncbi.nlm.nih.gov/pubmed/23467034). _Osteoarthritis and Cartilage_ 21(5): 756-63.  
  
N. Rodriguez, M. L. Oyen, **S. J. Shefelbine**, (2013). [Insight into differences in nanoindentation properties of bone](http://www.ncbi.nlm.nih.gov/pubmed/23262307). _J Mech Behavior of Biomed Mater_ 18: 90-9.  
  
G.N. Jones, D. Moschidou, K. Lay, H. Abdulrazzak, M. Vanleene, **S. J. Shefelbine**, J. Polak, P. de Coppi, N.M. Fisk, P.V. Guillot, (2012) [Upregulating CXCR4 in human fetal mesenchymal stem cells enhances engraftment and bone mechanics in a mouse model of osteogenesis imperfecta.](http://www.ncbi.nlm.nih.gov/pubmed/23197643) _Stem Cell Translational Medicine_ 1: 70-8.  
  
G.N. Jones, D. Moschidou, T.I. Puga-Iglesias, K. Kuleszewicz, M. Vanleene, **S. J. Shefelbine**, G. Bou-Gharios, N.M. Fisk, A.L. David, P. de Coppi, P.V. Guillot, (2012). [Ontological differences in first compared to third trimester human fetal placental chorionic stem cells.](http://www.ncbi.nlm.nih.gov/pubmed/22962584) _PloS One_: 7(9): e43395.  
  
M. Doube, S.C.W. Yen, M.M. Klosowski, A.A. Farke, J.R. Hutchinson, **S. J. Shefelbine**, (2012). [Whole-bone scaling of the avian pelvic limb.](http://www.ncbi.nlm.nih.gov/pubmed/22606941) _J Anatomy_ 221(1): 21-9.  
  
Carriero, A. Zavatsky, J. Stebbins, T. Theologis, G. Lenaerts, I. Jonkers, and **S. J. Shefelbine**, (2012). [Influence of altered gait patterns on the hip joint contact forces.](http://www.ncbi.nlm.nih.gov/pubmed/22587414) _Comput Meth in Biomech and Biomed Eng_ 17: 352-59.  
  
K.Y. Zhang, A. Wiktorowicz-Conroy, J.R. Hutchinson, M. Doube, M.M. Klosowski,**S. J. Shefelbine**, A.M.J. Bull, (2012). [3D morphometric and posture study of felid scapulae using statistical shape modelling.](http://www.ncbi.nlm.nih.gov/pubmed/22509335) _PLoS One_.2012;7(4): e34619  
  
M. Vanleene, A. Porter, P. Guillot, A. Boyde, M. Oyen, and **S. J. Shefelbine**, (2012). [Ultra-structural defects cause low bone matrix stiffness despite high mineralization in osteogenesis imperfecta mice.](http://www.ncbi.nlm.nih.gov/pubmed/22449447) _Bone_ 50(6):1317-23  
  
S. Cheng, **S. J. Shefelbine**, and M. Buehler, (2012). [Structural and mechanical differences between collagen homo- and heterotrimers: General insight and relevance for brittle bone disease.](http://www.ncbi.nlm.nih.gov/pubmed/22325288) _Biophysical Journal_ 102(3): 640-8.  
  
O. Panagiotopoulou, S. Wilshin, E. Rayfield, **S. J. Shefelbine**, J. R. Hutchinson, (2011). [What makes an accurate and reliable subject-specific finite element model? A case study of an elephant femur.](http://www.ncbi.nlm.nih.gov/pubmed/21752810) _J Royal Society Interface_ 9:351-61.  
  
M. Doube, M Kłosowski, A. M. Wiktorowicz-Conroy, J. R. Hutchinson, **S. J. Shefelbine,** (2011). [Trabecular bone scales allometrically in mammals and birds.](http://www.ncbi.nlm.nih.gov/pubmed/21389033) _Proceedings of the Royal Society Biology B_ 278:3067-73.  
  
J. P. Moodie, K. S.Stok, R. Müller, T. L.Vincent, **S. J. Shefelbine**, (2011). [Multimodal imaging demonstrates concomitant changes in bone and cartilage after destabilisation of the medial meniscus and increased joint laxity.](http://www.ncbi.nlm.nih.gov/pubmed/21094262) _Osteoarthritis and Cartilage_ 19(2): 163-70.  
  
M. Vanleene, Z. Saldanha, K. L. Cloyd, G. Jell, G. Bou-Gharios, J. H. D. Bassett, G. R. Williams, N. M. Fisk, M. L. Oyen, M. M. Stevens, P. V. Guillot, **S. J. Shefelbine,** (2011). [Transplantation of human fetal blood stem/stromal cells in the osteogenesis imperfecta mouse leads to improvement in multi-scale tissue properties.](http://www.ncbi.nlm.nih.gov/pubmed/21088133) _Blood_ 117(3): 1053-60.  
  
M. Doube, M. M. Kłosowski, I. Arganda-Carreras, F. P. Cordelières, R. P. Dougherty, J. S. Jackson, B. Schmid, J. R. Hutchinson, **S. J. Shefelbine**, (2010). [BoneJ: Free and extensible bone image analysis in ImageJ.](http://www.ncbi.nlm.nih.gov/pubmed/20817052) _Bone_ 47(6): 1076-9.  
  
B. Poulet, R. W. Hamilton, **S. J. Shefelbine**, A. A. Pitsillides, (2010). [Characterising a novel and adjustable non-invasive murine knee joint loading model.](http://www.ncbi.nlm.nih.gov/pubmed/20882669) _Arthritis Rheum_. 63(1): 137-4.  
  
A. Carriero, I. Jonkers, **S. J. Shefelbine,** (2010). [Mechanobiological prediction of proximal femoral deformities in children with cerebral palsy.](http://www.ncbi.nlm.nih.gov/pubmed/20229379) _Computer Methods and Biomedical Engineering_ 14(3): 253-62  
  
P. Sztefek, M. Vanleene, R. Olsson, R. Collinson, A. A. Pitsillides, **S. J. Shefelbine,** (2010). [Using digital image correlation to determine surface strains during loading and after adaptation of the mouse tibia.](http://www.ncbi.nlm.nih.gov/pubmed/20005517) _Journal of Biomechanics_ 43: 599-605.  
  
M. Doube, A. Wiktorowicz Conroy, P. Christiansen, JR Hutchinson, **S. J. Shefelbine,** (2009). [Three-dimensional geometric analysis of felid limb bone allometry.](http://www.ncbi.nlm.nih.gov/pubmed/19270749) _Public Library of Science (PLoS) One_. 4(3) e4742. (link to pdf)  
  
A. Carriero, A. Zavatsky, J. Stebbins, T. Theologis, **S. J. Shefelbine,** (2009). [Correlation between lower limb bone morphology and gait characteristics in children with spastic diplegic cerebral palsy.](http://www.ncbi.nlm.nih.gov/pubmed/19098651) _Journal of Paediatric Orthopaedics_. 29(1):73-9.  
  
A. Carriero, A. Zavatsky, J. Stebbins, T. Theologis, **S. J. Shefelbine,** (2009). [Determination of gait patterns in children with spastic diplegic cerebral palsy using principal components.](http://www.ncbi.nlm.nih.gov/pubmed/18676146) _Gait and Posture_. 29(1):71-5.  
  
R. D. Carpenter, **S. J. Shefelbine**, J. Lozano, K.Y. Lee, S. Majumdar, B. Ma, (2008). [A New Device for Measuring Knee Rotation in ACL-Deficient Patients.](http://medicaldevices.asmedigitalcollection.asme.org/article.aspx?articleid=1473870) _Journal of Medical Devices_, 2: 44501-1.  
  
P.V. Guillot, O. Abass, J.H. Bassett, **S. J. Shefelbine**, G. Bou-Gharios, J. Chan, H. Kurata, G.R. Williams, J. Polak, N.M. Fisk, (2008). [Intrauterine transplantation of human fetal mesenchymal stem cells from first-trimester blood repairs bone and reduces fractures in osteogenesis imperfecta mice.](http://www.ncbi.nlm.nih.gov/pubmed/17967940) _Blood_, 111: 1717 – 1725.  
  
**S. J. Shefelbine**, S. Majumdar, (2005). [Imaging bone structure and osteoporosis using MRI.](http://www.ccmbm.com/index.php?PAGE=articolo_dett&ID_ISSUE=92&id_article=760) _Clincal Cases in Min and Bone Metabolism_ 2(2):119-26.  
  
**S. J. Shefelbine**, B. Ma, K.Y. Lee, M.A. Schrumpf, P. Patel, M.R. Safran, J.P. Slavinsky, S. Majumdar, (2006). [MRI Analysis of in vivo meniscal and tibiofemoral kinematics in ACL-deficient and normal knees.](http://www.ncbi.nlm.nih.gov/pubmed/16652339) _Journal of Orthpaedic Research_. 24(6): 1208-17.  
  
Sarkar, M. R., P. Augat, **S. J. Shefelbine**, S. Schorlemmer, M. Huber-Lang, L. Claes, L. Kinzl, A. Ignatius, (2005). [Bone formation in a long bone defect model using a platelet-rich plasma-loaded collagen scaffold.](http://www.ncbi.nlm.nih.gov/pubmed/16307796) _Biomaterials_ 27(9): 1817-23.  
  
**S. J. Shefelbine**, P. Augat, L. Claes, and U. Simon, (2005). [Trabecular bone fracture healing simulation with finite element analysis and fuzzy logic.](http://www.ncbi.nlm.nih.gov/pubmed/16214492) _Journal of Biomechanics_ 38(12): 2440-50.  
  
**S. J. Shefelbine**, P. Augat, L. Claes, A. Gold, Y. Gabet, I. Bab, and R. Müller, (2005). [Prediction of fracture callus mechanical properties using microCT images and finite element analysis.](http://www.ncbi.nlm.nih.gov/pubmed/15777656) _Bone_ 36(3): 480-8.  
  
**S. J. Shefelbine**, P. Augat, L. Claes, and A. Beck, (2005). [Intact fibula improves fracture healing in a rat tibia osteotomy model.](http://www.ncbi.nlm.nih.gov/pubmed/15734267) _Journal of Orthopaedic Research_ 23(2): 489-93.  
  
**S. J. Shefelbine** and D. R. Carter, (2004). [Mechanobiological predictions of femoral anteversion in cerebral palsy.](http://www.ncbi.nlm.nih.gov/pubmed/15008378) _Annals of Biomedical Engineering_. 32(2): 297-305.  
  
**S. J. Shefelbine** and D. R. Carter, (2004). [Mechanobiological predictions of growth front morphology in developmental hip dysplasia.](http://www.ncbi.nlm.nih.gov/pubmed/15013095) _Journal of Orthopedic Research_ 22(2): 346-52.  
  
**S. J. Shefelbine**, C. Tardieu, and D. R. Carter, (2002). [Development of the femoral bicondylar angle in hominid bipedalism.](http://www.ncbi.nlm.nih.gov/pubmed/11996917) _Bone_ 30(5): 765-70.  
  
  

### Books

  
A.E. Draghici, **S. J. Shefelbine**, (2016). [Increased Bone Fracture After SCI: Can Exercise Reduce Risk?](https://link.springer.com/chapter/10.1007%2F978-1-4939-6664-6_8) _The Physiology of Exercise in Spinal Cord Injury_, 161-174.  
  
**S. J. Shefelbine**, J. Clarkson, R. Farmer, S. Eason. _Good design practice for medical devices and equipment – requirements capture_. Institute for Manufact
